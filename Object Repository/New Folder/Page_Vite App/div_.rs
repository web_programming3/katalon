<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_</name>
   <tag></tag>
   <elementGuidId>50e3c056-820d-447c-bf2f-19e18a4a445a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.v-list-item-title</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='ข้อมูลส่วนตัว'])[1]/preceding::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a612efbb-1850-4203-93c3-41e6ea8e6e5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>v-list-item-title</value>
      <webElementGuid>cba1ffd1-1b22-404a-9aed-099741794057</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>ประวัติการจอง</value>
      <webElementGuid>ce35832a-d0e5-4639-beda-01b225e7d65c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;v-overlay-container&quot;]/div[@class=&quot;v-overlay v-overlay--absolute v-overlay--active v-theme--light v-locale--is-ltr v-menu&quot;]/div[@class=&quot;v-overlay__content&quot;]/div[@class=&quot;v-list v-theme--light v-list--density-default v-list--one-line&quot;]/div[@class=&quot;v-list-item v-list-item--link v-theme--light v-list-item--density-default v-list-item--one-line v-list-item--variant-text&quot;]/div[@class=&quot;v-list-item__content&quot;]/div[@class=&quot;v-list-item-title&quot;]</value>
      <webElementGuid>a06d55f2-a34d-4a2d-a124-cd8a093f8467</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ข้อมูลส่วนตัว'])[1]/preceding::div[1]</value>
      <webElementGuid>9cd20d70-76a5-453b-95c5-a75745b78fe6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ตรวจสอบตั๋ว/รับอาหารเครื่องดื่ม'])[1]/preceding::div[4]</value>
      <webElementGuid>dc21739f-302b-4081-917d-fc1ccfc37270</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='ประวัติการจอง']/parent::*</value>
      <webElementGuid>e20903a3-b892-4a3a-ae5c-8be333485560</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div/div/div</value>
      <webElementGuid>bd172eef-31fa-491b-9092-015731c078ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'ประวัติการจอง' or . = 'ประวัติการจอง')]</value>
      <webElementGuid>31816def-ef18-48e1-a3b5-6075002b559b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
