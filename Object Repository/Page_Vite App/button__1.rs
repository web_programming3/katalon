<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button__1</name>
   <tag></tag>
   <elementGuidId>a1ebb500-d89e-4fa0-85b9-c14329f727e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[7]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a488c5eb-fac7-4bce-9a33-9b60e1807c5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>733c80ad-4e55-4b56-bfe9-56fc1045ea6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>v-btn v-btn--elevated v-theme--light v-btn--density-default rounded-xl v-btn--size-default v-btn--variant-elevated mt-6 ml-2</value>
      <webElementGuid>dbc22aee-1651-4aa2-99e1-d174c03132ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>รีวิว</value>
      <webElementGuid>e0b81713-83a3-4d85-a6f7-eea3642ef3c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;v-layout&quot;]/main[@class=&quot;v-main&quot;]/div[@class=&quot;v-container v-container--fluid v-locale--is-ltr&quot;]/div[@class=&quot;v-row&quot;]/div[@class=&quot;v-col v-col-3 d-flex flex-column mt-12 ml-7&quot;]/div[@class=&quot;v-row&quot;]/div[@class=&quot;v-col&quot;]/button[@class=&quot;v-btn v-btn--elevated v-theme--light v-btn--density-default rounded-xl v-btn--size-default v-btn--variant-elevated mt-6 ml-2&quot;]</value>
      <webElementGuid>8a0189d7-6afb-45ca-9612-f42ca5dde61b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[7]</value>
      <webElementGuid>f75cc30a-a0f7-44c0-a2ed-abf86d4145b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/main/div/div[3]/div[2]/div/div[2]/button</value>
      <webElementGuid>3668f69a-eac2-425d-9d99-a536543e2c22</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(ยังไม่ได้รับ)'])[3]/following::button[2]</value>
      <webElementGuid>d6fa941c-b707-45de-aa67-106fda67b860</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/div/div[2]/button</value>
      <webElementGuid>21c0e276-5c86-4edd-8147-1a8ba24c38f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'รีวิว' or . = 'รีวิว')]</value>
      <webElementGuid>c48a906d-d582-404a-961f-247f9dc312ea</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
