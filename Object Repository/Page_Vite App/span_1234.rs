<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_1234</name>
   <tag></tag>
   <elementGuidId>b549f5fd-3627-4e26-bf4e-6d34f5d5ce67</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.v-btn__content</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/header/div/div/div[3]/button/span[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>c52250ab-53e2-4af3-a8db-2331f8b3da41</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>v-btn__content</value>
      <webElementGuid>b71c098e-6910-47f7-ad38-67ba2b8545e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>ตี๋ 1234</value>
      <webElementGuid>a0be9631-c82b-4cdc-bafd-b1d02df004e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;v-layout&quot;]/header[@class=&quot;v-toolbar v-toolbar--density-default elevation-2 v-theme--light v-locale--is-ltr v-app-bar&quot;]/div[@class=&quot;v-toolbar__content&quot;]/div[@class=&quot;v-row&quot;]/div[@class=&quot;v-col v-col-4&quot;]/button[@class=&quot;v-btn v-theme--light v-btn--density-default rounded-xl v-btn--size-default v-btn--variant-text&quot;]/span[@class=&quot;v-btn__content&quot;]</value>
      <webElementGuid>369d6262-7823-4598-b41b-4ede63b2432d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/header/div/div/div[3]/button/span[4]</value>
      <webElementGuid>58222cae-fec9-4547-ba89-b2e01705e667</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='ตี๋ 1234']/parent::*</value>
      <webElementGuid>42db531a-4db8-44a4-822f-4453d05cd32e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[4]</value>
      <webElementGuid>f6868c03-2c69-4807-be8d-9b5a97cd5de7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'ตี๋ 1234' or . = 'ตี๋ 1234')]</value>
      <webElementGuid>2eca71b5-8abc-4273-b05d-af05b8863cfe</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
