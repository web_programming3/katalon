<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_ActionAdventureFantasy190 (1)</name>
   <tag></tag>
   <elementGuidId>55f94b0c-878e-4209-99c8-f0cf53d56489</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.v-card.v-card--link.v-theme--light.v-card--density-default.elevation-10.v-card--variant-elevated.ma-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/main/div/div[2]/div/div[3]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>2dcf8995-d847-4f7d-9eae-87d33b45afd5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>v-card v-card--link v-theme--light v-card--density-default elevation-10 v-card--variant-elevated ma-5</value>
      <webElementGuid>1bb29478-bf8e-470a-b335-f88de347ea19</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>อวตาร : วิถีแห่งสายน้ำ
Action/Adventure/Fantasy190 นาที คลิกเพื่อดูเพิ่มเติม </value>
      <webElementGuid>a5fc5fed-36c5-4ca5-98f9-4a8e864e593d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;v-layout&quot;]/main[@class=&quot;v-main&quot;]/div[@class=&quot;v-container v-container--fluid v-locale--is-ltr&quot;]/div[@class=&quot;v-container v-container--fluid v-locale--is-ltr&quot;]/div[@class=&quot;v-row&quot;]/div[@class=&quot;v-col v-col-3&quot;]/div[@class=&quot;v-card v-card--link v-theme--light v-card--density-default elevation-10 v-card--variant-elevated ma-5&quot;]</value>
      <webElementGuid>86339e00-ff28-4740-a565-c5a43cde4db6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/main/div/div[2]/div/div[3]/div</value>
      <webElementGuid>4bfcf873-5be4-4d32-92a5-629a16621535</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div</value>
      <webElementGuid>fb4fd3e9-dae4-420a-ba25-43f0df053ef9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'อวตาร : วิถีแห่งสายน้ำ
Action/Adventure/Fantasy190 นาที คลิกเพื่อดูเพิ่มเติม ' or . = 'อวตาร : วิถีแห่งสายน้ำ
Action/Adventure/Fantasy190 นาที คลิกเพื่อดูเพิ่มเติม ')]</value>
      <webElementGuid>23a92c7d-2f6c-40bb-a272-7c3a6d28e51f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
