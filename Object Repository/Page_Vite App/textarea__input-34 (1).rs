<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea__input-34 (1)</name>
   <tag></tag>
   <elementGuidId>e351c715-730e-460c-823c-7a3285368a57</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#input-34</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id='input-34']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>7741f87b-7b0a-4f8c-a62c-0b7cf4d2e3b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>v-field__input</value>
      <webElementGuid>3a5b1650-0f8f-494d-8cc9-a8dcd9d33488</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>rows</name>
      <type>Main</type>
      <value>5</value>
      <webElementGuid>751cf862-e9b6-4af5-9bee-80004e0270c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>input-34</value>
      <webElementGuid>e47789bc-90fa-4f59-a456-4491262bdea5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>input-34-messages</value>
      <webElementGuid>8ff8767b-afb5-4543-8692-271d3d228021</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>250</value>
      <webElementGuid>b8499f80-16d3-4568-8e19-1ed1a5f43d66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;input-34&quot;)</value>
      <webElementGuid>61d02467-8b8d-454a-a93a-16f825dd11aa</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//textarea[@id='input-34']</value>
      <webElementGuid>9c1a004b-1055-4966-a7d7-6b65083351db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/main/div/div/div[2]/div/div[2]/div/div/div[3]/textarea</value>
      <webElementGuid>a9fbd629-c6ac-4711-829a-7abf2f32e0b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>69ceacdd-4987-408a-83f7-fe07c020d059</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@id = 'input-34']</value>
      <webElementGuid>26200db8-d5bd-402d-a1d9-c180d75068d3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
