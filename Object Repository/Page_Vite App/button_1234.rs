<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_1234</name>
   <tag></tag>
   <elementGuidId>c7ba4d56-a806-40fa-ba04-f0f10260a73a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.v-btn.v-theme--light.v-btn--density-default.rounded-xl.v-btn--size-default.v-btn--variant-text</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>8636092b-93a5-4ccd-9856-ff37738fc938</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>78aec695-6d0c-481e-8562-89c2567e7d40</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>v-btn v-theme--light v-btn--density-default rounded-xl v-btn--size-default v-btn--variant-text</value>
      <webElementGuid>9e1ff476-4d9c-4aee-a39a-49e8715c9102</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>targetref</name>
      <type>Main</type>
      <value>[object Object]</value>
      <webElementGuid>31744a71-730d-48e7-9f11-7687864c002d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>menu</value>
      <webElementGuid>6864741e-738c-4677-b42c-565d64e0b578</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>d02b6954-c2bb-4c24-8ed8-8961d5feacb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-owns</name>
      <type>Main</type>
      <value>v-menu-1</value>
      <webElementGuid>a8b7970f-a875-4e28-82e0-7e6af5dd86ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>ตี๋ 1234</value>
      <webElementGuid>d1d53c59-4c15-4a3d-b555-1e41139639d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;v-layout&quot;]/header[@class=&quot;v-toolbar v-toolbar--density-default elevation-2 v-theme--light v-locale--is-ltr v-app-bar&quot;]/div[@class=&quot;v-toolbar__content&quot;]/div[@class=&quot;v-row&quot;]/div[@class=&quot;v-col v-col-4&quot;]/button[@class=&quot;v-btn v-theme--light v-btn--density-default rounded-xl v-btn--size-default v-btn--variant-text&quot;]</value>
      <webElementGuid>b406d5f1-544c-4573-8d02-fc6d4946d5c6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='button']</value>
      <webElementGuid>192db40e-6c62-4e81-831c-7ad5cd9191ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/header/div/div/div[3]/button</value>
      <webElementGuid>1ee5c2f5-03aa-46aa-a46a-883a61c5a528</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>955e7b95-0ce4-4e71-8190-344beed68f36</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'ตี๋ 1234' or . = 'ตี๋ 1234')]</value>
      <webElementGuid>26f0c002-d6fa-4dd3-ac04-66a3fc296cdc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
