<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg__v-icon__svg (1)</name>
   <tag></tag>
   <elementGuidId>0592009c-f850-483e-9455-3609492f783f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.v-btn__content > i.v-icon.notranslate.v-theme--light.text-red > svg.v-icon__svg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='เลือกวิธีการชำระเงิน'])[1]/following::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>665e43ae-201c-40d6-ab12-59d5ff0ba6f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>v-icon__svg</value>
      <webElementGuid>80f8fb32-b017-4b5a-9dee-152d84c8d7f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns</name>
      <type>Main</type>
      <value>http://www.w3.org/2000/svg</value>
      <webElementGuid>b4eff11b-d144-428e-ad41-830a030ee80c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 24 24</value>
      <webElementGuid>564a6dc4-a199-4e51-9f63-1fbfac8c2178</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>e398f3c8-faa8-41e4-ae7d-f08974cad566</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>d21c07b5-d781-474a-aed5-e90d1d41df49</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;v-layout&quot;]/main[@class=&quot;v-main&quot;]/div[@class=&quot;v-container v-container--fluid v-locale--is-ltr&quot;]/div[@class=&quot;v-sheet v-theme--light v-stepper v-stepper--alt-labels&quot;]/div[@class=&quot;v-window v-theme--light v-stepper-window&quot;]/div[@class=&quot;v-window__container&quot;]/div[@class=&quot;v-window-item v-window-item--active v-stepper-window-item&quot;]/div[@class=&quot;v-card v-theme--light v-card--density-default rounded-lg v-card--variant-outlined pa-10&quot;]/div[@class=&quot;v-card v-card--flat v-theme--light v-card--density-default v-card--variant-elevated&quot;]/div[@class=&quot;v-row mt-3&quot;]/div[@class=&quot;v-col&quot;]/button[@class=&quot;v-btn v-btn--stacked v-theme--light v-btn--density-default v-btn--size-default v-btn--variant-outlined&quot;]/span[@class=&quot;v-btn__content&quot;]/i[@class=&quot;v-icon notranslate v-theme--light text-red&quot;]/svg[@class=&quot;v-icon__svg&quot;]</value>
      <webElementGuid>f11fc375-766d-42e5-bd92-45205237104f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='เลือกวิธีการชำระเงิน'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>215ab355-eb58-4ad3-8e2e-5bd46a2b472a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ยืนยันการซื้อตั๋วรับชมภาพยนตร์'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>b757dc8e-2786-4517-b27f-8f444920155b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='บัตรเครดิต / บัตรเดบิต'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>717475f4-cfaa-4c1c-8d68-690252b45d46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='QR-Payment'])[1]/preceding::*[name()='svg'][2]</value>
      <webElementGuid>ab3df91d-28fc-4ec5-9577-a1879caa34c9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
