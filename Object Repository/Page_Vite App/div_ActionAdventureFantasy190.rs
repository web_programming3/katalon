<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_ActionAdventureFantasy190</name>
   <tag></tag>
   <elementGuidId>184afe93-7917-45d5-a02f-64f1f9818ccc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.v-card.v-card--link.v-theme--light.v-card--density-default.elevation-10.v-card--variant-elevated.ma-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/main/div/div[2]/div/div[3]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>44752ba2-42c4-40db-84c3-0c36d63bc149</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>v-card v-card--link v-theme--light v-card--density-default elevation-10 v-card--variant-elevated ma-5</value>
      <webElementGuid>461b5f96-a79d-40bc-b684-78e5f769c90d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>อวตาร : วิถีแห่งสายน้ำ
Action/Adventure/Fantasy190 นาที คลิกเพื่อดูเพิ่มเติม </value>
      <webElementGuid>9c19eab4-b6ce-4d8c-a20c-67e4dbb87528</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;v-layout&quot;]/main[@class=&quot;v-main&quot;]/div[@class=&quot;v-container v-container--fluid v-locale--is-ltr&quot;]/div[@class=&quot;v-container v-container--fluid v-locale--is-ltr&quot;]/div[@class=&quot;v-row&quot;]/div[@class=&quot;v-col v-col-3&quot;]/div[@class=&quot;v-card v-card--link v-theme--light v-card--density-default elevation-10 v-card--variant-elevated ma-5&quot;]</value>
      <webElementGuid>af4af028-1332-4c1f-88a4-9d63f3068cf4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/main/div/div[2]/div/div[3]/div</value>
      <webElementGuid>a67a87f9-93f3-47d5-bd6c-42a86f83085f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div</value>
      <webElementGuid>4b0942fb-1733-477e-abc7-56515fe792d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'อวตาร : วิถีแห่งสายน้ำ
Action/Adventure/Fantasy190 นาที คลิกเพื่อดูเพิ่มเติม ' or . = 'อวตาร : วิถีแห่งสายน้ำ
Action/Adventure/Fantasy190 นาที คลิกเพื่อดูเพิ่มเติม ')]</value>
      <webElementGuid>f109650d-663d-48d5-a79c-2297c58d6b59</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
