import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

'เปิดหน้า Browser'
WebUI.navigateToUrl('http://localhost:5173/')

'เข้าสู่ระบบ'
WebUI.click(findTestObject('Object Repository/Page_Vite App/span_ (4) (1)'))

WebUI.setText(findTestObject('Object Repository/Page_Vite App/input__input-5 (3) (1)'), '1234@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Vite App/input__input-7 (2) (1)'), 'rsgGh7fdu5Lmy4zSHHzemA==')

'เข้าสู่ระบบ'
WebUI.click(findTestObject('Object Repository/Page_Vite App/span__1 (2) (1)'))

'กดชื่อผู้ใช้'
WebUI.click(findTestObject('Object Repository/Page_Vite App/span_1234 (1)'))

'กดปุ่มประวัติการจอง'
WebUI.click(findTestObject('Object Repository/Page_Vite App/div_ (1) (1)'))

'รีวิว'
WebUI.click(findTestObject('Object Repository/Page_Vite App/span__1_2 (1)'))

'รีวิววองก้า'
WebUI.click(findTestObject('Object Repository/Page_Vite App/button_ (2) (1)'))

'กรอกความคิดเห็น\r\n'
WebUI.setText(findTestObject('Object Repository/Page_Vite App/textarea__input-34 (1)'), 'เรื่องนี้สนุกมากๆครับ')

