import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

'เข้าสู่Path\r\n'
WebUI.navigateToUrl('http://localhost:5173/')

'กดปุ่มเพื่อเข้าสู่ระบบ'
WebUI.click(findTestObject('Object Repository/Movie/Page_Vite App/span_'))

'กรอกอีเมล'
WebUI.setText(findTestObject('Object Repository/Movie/Page_Vite App/input__input-5'), '1234@gmail.com')

'กรอกรหัสผ่าน'
WebUI.setEncryptedText(findTestObject('Object Repository/Movie/Page_Vite App/input__input-7'), 'rsgGh7fdu5Lmy4zSHHzemA==')

'เข้าสู่ระบบ'
WebUI.click(findTestObject('Object Repository/Movie/Page_Vite App/span__1'))

'Avatan'
WebUI.click(findTestObject('Object Repository/Page_Vite App/div_ActionAdventureFantasy190'))

'เวลา 16:00'
WebUI.click(findTestObject('Object Repository/Movie/Page_Vite App/button_1600'))

'ที่นั่ง C9'
WebUI.click(findTestObject('Object Repository/Page_Vite App/svg_Sofa Sweet(Pair)_v-icon__svg (1)'))

'ที่นั่ง C9'
WebUI.click(findTestObject('Object Repository/Page_Vite App/svg_Sofa Sweet(Pair)_v-icon__svg (1)'))

'ที่นั่ง C9'
WebUI.click(findTestObject('Object Repository/Page_Vite App/svg_Sofa Sweet(Pair)_v-icon__svg (1)'))

'ซื้ออาหารเพิ่ม'
WebUI.click(findTestObject('Object Repository/Movie/Page_Vite App/span__1_2'))

'ป็อบคอน'
WebUI.click(findTestObject('Object Repository/Movie/Page_Vite App/div_()150'))

'น้ำโค้ก'
WebUI.click(findTestObject('Object Repository/Movie/Page_Vite App/div_44 Oz.70'))

'ชำระเงืน'
WebUI.click(findTestObject('Object Repository/Movie/Page_Vite App/span__1_2_3'))

'QR Code'
WebUI.click(findTestObject('Object Repository/Movie/Page_Vite App/svg__v-icon__svg'))

'ชำระเงิน'
WebUI.click(findTestObject('Object Repository/Movie/Page_Vite App/span__1_2_3_4'))

