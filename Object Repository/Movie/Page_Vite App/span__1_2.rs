<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span__1_2</name>
   <tag></tag>
   <elementGuidId>df03abda-258b-4fc2-a863-d50268d591bf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.v-btn.v-btn--elevated.v-theme--light.v-btn--density-default.elevation-0.rounded-lg.v-btn--size-large.v-btn--variant-elevated.mb-2 > span.v-btn__content</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/main/div/div/div[3]/div/div[2]/div/div[2]/div[2]/div/div[5]/div/div/button/span[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>b9883505-7456-4adf-a635-e5dbf416bbbb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>v-btn__content</value>
      <webElementGuid>97fe0d9d-a878-4d7f-bd62-2b00394fdb54</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>ซื้ออาหาร / เครื่องดื่ม</value>
      <webElementGuid>6dbfe22d-a40f-4690-9514-895d34fca727</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;v-layout&quot;]/main[@class=&quot;v-main&quot;]/div[@class=&quot;v-container v-container--fluid v-locale--is-ltr&quot;]/div[@class=&quot;v-sheet v-theme--light v-stepper v-stepper--alt-labels&quot;]/div[@class=&quot;v-window v-theme--light v-stepper-window&quot;]/div[@class=&quot;v-window__container&quot;]/div[@class=&quot;v-window-item v-window-item--active v-stepper-window-item&quot;]/div[@class=&quot;v-card v-theme--light v-card--density-default v-card--variant-elevated&quot;]/div[@class=&quot;v-row&quot;]/div[@class=&quot;v-col&quot;]/div[@class=&quot;v-card v-theme--light v-card--density-default elevation-0 v-card--variant-elevated mt-5 ml-5 pa-5&quot;]/div[@class=&quot;v-row&quot;]/div[@class=&quot;v-col mt-9&quot;]/div[@class=&quot;v-card v-theme--light v-card--density-default elevation-0 v-card--variant-elevated d-flex flex-column pa-5&quot;]/button[@class=&quot;v-btn v-btn--elevated v-theme--light v-btn--density-default elevation-0 rounded-lg v-btn--size-large v-btn--variant-elevated mb-2&quot;]/span[@class=&quot;v-btn__content&quot;]</value>
      <webElementGuid>5a0574da-1c42-41e7-9262-47f80cfb9e76</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/main/div/div/div[3]/div/div[2]/div/div[2]/div[2]/div/div[5]/div/div/button/span[3]</value>
      <webElementGuid>073d082e-084f-478c-a224-79fccb2e39e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ราคารวม'])[1]/following::span[3]</value>
      <webElementGuid>b9f2078c-8128-4ba7-aca9-e89e54e92917</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='L11'])[1]/following::span[3]</value>
      <webElementGuid>ff5e3738-d9aa-4dd7-91d3-5c5039e8b9a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='ซื้ออาหาร / เครื่องดื่ม']/parent::*</value>
      <webElementGuid>0063d111-4d2b-44bb-9916-3fe133cfe982</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/button/span[3]</value>
      <webElementGuid>0ed32e00-3844-431f-a00f-3d6caf5bb0b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'ซื้ออาหาร / เครื่องดื่ม' or . = 'ซื้ออาหาร / เครื่องดื่ม')]</value>
      <webElementGuid>227c9db1-5b58-44be-a5fd-a34ae8ed2cc3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
