<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__input-7</name>
   <tag></tag>
   <elementGuidId>a17e6ef8-817d-45f3-9e9a-6e312cdde336</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#input-7</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='input-7']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>830e6fdb-f2af-4bc4-acf7-cd3a36991a8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>size</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>d73cbb58-d5e9-4a0c-b90f-24edbd15e9ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>5f0807ec-c77c-42d0-817f-260e56409521</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>input-7</value>
      <webElementGuid>a2ae68d7-85e8-4797-a12c-e4c63677a31d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>input-7-messages</value>
      <webElementGuid>215a4e71-2b2f-4e54-8723-10776dcbec59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>v-field__input</value>
      <webElementGuid>929e8911-c75d-4d38-baf8-f665900857dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;input-7&quot;)</value>
      <webElementGuid>c2fd3a1c-65c9-46a0-b07d-4f74c58eb438</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='input-7']</value>
      <webElementGuid>8dc22252-6c36-4ce2-9b52-6ef94aef9fd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[4]/input</value>
      <webElementGuid>146e4fa7-b8ce-487a-a278-53684c6193dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'password' and @id = 'input-7']</value>
      <webElementGuid>2d14c552-d218-4fd6-8fc3-a665dce3e06f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
