<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_ActionAdventureFantasy190</name>
   <tag></tag>
   <elementGuidId>cb58b7b4-64b1-416f-9d27-bc3d194465ad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.v-sheet.v-theme--light.pa-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/main/div/div[2]/div/div[3]/div/div[2]/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3f4a8923-3f23-4775-a09a-9a9e33a14a58</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>v-sheet v-theme--light pa-4</value>
      <webElementGuid>a4fd585d-bfb0-467c-840e-734a99d689b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>อวตาร : วิถีแห่งสายน้ำ
Action/Adventure/Fantasy190 นาที คลิกเพื่อดูเพิ่มเติม </value>
      <webElementGuid>2be72282-a255-482b-88bd-089813511114</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;v-layout&quot;]/main[@class=&quot;v-main&quot;]/div[@class=&quot;v-container v-container--fluid v-locale--is-ltr&quot;]/div[@class=&quot;v-container v-container--fluid v-locale--is-ltr&quot;]/div[@class=&quot;v-row&quot;]/div[@class=&quot;v-col v-col-3&quot;]/div[@class=&quot;v-card v-card--link v-theme--light v-card--density-default elevation-10 v-card--variant-elevated ma-5&quot;]/div[@class=&quot;v-responsive v-img&quot;]/div[@class=&quot;v-responsive__content&quot;]/div[@class=&quot;v-sheet v-theme--light pa-4&quot;]</value>
      <webElementGuid>0dba6790-ccfd-473a-8f95-9eeb4c7a0c87</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/main/div/div[2]/div/div[3]/div/div[2]/div[2]/div</value>
      <webElementGuid>a76e63ba-01f0-48fb-9a75-79e5368d3aa2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div</value>
      <webElementGuid>d0bd1fd6-edc1-4592-b9ea-3e1b2d131dc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'อวตาร : วิถีแห่งสายน้ำ
Action/Adventure/Fantasy190 นาที คลิกเพื่อดูเพิ่มเติม ' or . = 'อวตาร : วิถีแห่งสายน้ำ
Action/Adventure/Fantasy190 นาที คลิกเพื่อดูเพิ่มเติม ')]</value>
      <webElementGuid>809fcc7b-9685-4408-8584-b67a980ddf21</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
