<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__input-5</name>
   <tag></tag>
   <elementGuidId>01d6a283-bf90-4fed-971f-9389d0b01ee9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#input-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='input-5']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>59ffa98e-0d68-4e00-9c70-c8e45caddfaa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>size</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>b7598ef0-d04e-4141-b0cc-f759e894e4a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>15b36744-d83b-4125-9e65-c959bd73a716</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>input-5</value>
      <webElementGuid>b228a806-49a6-48fb-afa0-febf03e9b126</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>input-5-messages</value>
      <webElementGuid>080736d3-8155-4274-89a5-6fc723506fbc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>v-field__input</value>
      <webElementGuid>5e939fbe-42a6-47cd-8d78-6cadcf3be3e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;input-5&quot;)</value>
      <webElementGuid>03a4344c-2ca7-4814-bb0f-c7deba0592b3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='input-5']</value>
      <webElementGuid>5c1fd913-60c1-4232-96fb-e2d20c247d88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>4babb3e3-e706-4da8-b33f-a0d23c0aa3c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @id = 'input-5']</value>
      <webElementGuid>d702e7e6-0605-47ba-9df7-5980ae33d50c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
