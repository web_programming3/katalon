<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path</name>
   <tag></tag>
   <elementGuidId>8ba84314-e335-461e-adfa-97db0e4e071d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.v-btn__content > i.v-icon.notranslate.v-theme--light.text-red > svg.v-icon__svg > path</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
      <webElementGuid>77d61ee6-2c36-4a27-afab-87c7ab807c68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M20,8H4V6H20M20,18H4V12H20M20,4H4C2.89,4 2,4.89 2,6V18A2,2 0 0,0 4,20H20A2,2 0 0,0 22,18V6C22,4.89 21.1,4 20,4Z</value>
      <webElementGuid>9954f317-bb70-46f4-b89b-3af8ddc3d2eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;v-layout&quot;]/main[@class=&quot;v-main&quot;]/div[@class=&quot;v-container v-container--fluid v-locale--is-ltr&quot;]/div[@class=&quot;v-sheet v-theme--light v-stepper v-stepper--alt-labels&quot;]/div[@class=&quot;v-window v-theme--light v-stepper-window&quot;]/div[@class=&quot;v-window__container&quot;]/div[@class=&quot;v-window-item v-window-item--active v-stepper-window-item&quot;]/div[@class=&quot;v-card v-theme--light v-card--density-default rounded-lg v-card--variant-outlined pa-10&quot;]/div[@class=&quot;v-card v-card--flat v-theme--light v-card--density-default v-card--variant-elevated&quot;]/div[@class=&quot;v-row mt-3&quot;]/div[@class=&quot;v-col&quot;]/button[@class=&quot;v-btn v-btn--stacked v-theme--light v-btn--density-default v-btn--size-default v-btn--variant-outlined&quot;]/span[@class=&quot;v-btn__content&quot;]/i[@class=&quot;v-icon notranslate v-theme--light text-red&quot;]/svg[@class=&quot;v-icon__svg&quot;]/path[1]</value>
      <webElementGuid>18d79808-f5bb-4b12-93b7-4f9e117fc384</webElementGuid>
   </webElementProperties>
</WebElementEntity>
