import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://localhost:5173/')

WebUI.click(findTestObject('Object Repository/Page_Vite App/span_ (2) (1)'))

WebUI.setText(findTestObject('Object Repository/Page_Vite App/input__input-5 (2) (1)'), '1234@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Vite App/input__input-7 (1) (1)'), 'rsgGh7fdu5Lmy4zSHHzemA==')

WebUI.click(findTestObject('Object Repository/Page_Vite App/span__1 (1) (1)'))

WebUI.click(findTestObject('Object Repository/Page_Vite App/div_ActionAdventureFantasy190 (1)'))

WebUI.openBrowser('')

WebUI.navigateToUrl('http://localhost:5173/')

WebUI.click(findTestObject('Object Repository/Page_Vite App/span_ (3)'))

WebUI.click(findTestObject('Object Repository/Page_Vite App/path'))

WebUI.click(findTestObject('Object Repository/Page_Vite App/span_ (3)'))

WebUI.click(findTestObject('Object Repository/Page_Vite App/button_ (1)'))

WebUI.click(findTestObject('Object Repository/Page_Vite App/span_ (3)'))

WebUI.click(findTestObject('Object Repository/Page_Vite App/path'))

WebUI.click(findTestObject('Object Repository/Page_Vite App/svg__v-icon__svg (1)'))

