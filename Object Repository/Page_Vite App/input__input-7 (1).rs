<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__input-7 (1)</name>
   <tag></tag>
   <elementGuidId>b58150ab-455b-4b3a-8220-f85049ae7627</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#input-7</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='input-7']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>300a92e8-c209-422b-8def-e6600db1618c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>size</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>4def55cc-d378-43df-bb74-76ea4310f5ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>5597ae51-97e5-401b-9a1b-e5d472173bfb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>input-7</value>
      <webElementGuid>913bd705-64f4-421a-84d5-613a199b665b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>input-7-messages</value>
      <webElementGuid>d16dc04c-f580-4624-b075-4092bc9228db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>v-field__input</value>
      <webElementGuid>e5a26974-e1cd-4141-a0e0-068916732519</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;input-7&quot;)</value>
      <webElementGuid>f4073f50-75a7-4cac-bee6-87a3121b5451</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='input-7']</value>
      <webElementGuid>0ac8c520-71d9-44cb-8b7e-eace07ca23f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[4]/input</value>
      <webElementGuid>e6804cc2-efa3-4563-aa26-7ba54c59c69e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'password' and @id = 'input-7']</value>
      <webElementGuid>08ea50c5-6c2e-4f66-9364-b0c089762dd5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
